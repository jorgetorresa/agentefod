import os
import socket
import logging
from git import Repo


def main():
    logging.basicConfig(filename='logAgenteGit.log',level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('Inicio obtencion del repositorio')
    getRepo()
    logging.info('Fin de la obtencion del repositorio')

def internet(host="8.8.8.8", port=53, timeout=3):
  """
  Host: 8.8.8.8 (google-public-dns-a.google.com)
  OpenPort: 53/tcp
  Service: domain (DNS/TCP)
  """
  try:
    socket.setdefaulttimeout(timeout)
    socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
    logging.info('Conexion a internet exitosa')
    return True
  except socket.error as ex:
    print(ex)
    loggin.warning('Error de conexion')
    logging.error(ex)
    return False

def getRepo():
    if(internet()):
        if(checkDir()):
            repo = Repo.clone_from('https://jorgetorresa@bitbucket.org/jorgetorresa/agentefodwindows.git', 'agentefod')           
            logging.info('Git clone done')
        else:
            repo = Repo('agentefod')
            repo.remotes.origin.pull()
            logging.info('Git pull done')

def checkDir():
    if os.path.exists('agentefod'):
        logging.warning('Directorio agentefod existente')
        return False
    else:
        logging.warning('Directorio agente fod faltante')
        return True

if __name__ == '__main__':
    main()
